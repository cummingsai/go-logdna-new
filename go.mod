module bitbucket.org/cummingsai/go-logdna-new

go 1.14

require (
	github.com/evalphobia/httpwrapper v0.2.1
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
	gopkg.in/eapache/go-resiliency.v1 v1.2.0 // indirect
	gopkg.in/h2non/gentleman-retry.v2 v2.0.1 // indirect
	gopkg.in/h2non/gentleman.v2 v2.0.4 // indirect
)
